import 'package:flutter/material.dart';
import 'package:flutter_youbike/models/youbike_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

class ListDetailPage extends StatefulWidget {
  final RetVal model;

  ListDetailPage({required this.model});

  @override
  State<StatefulWidget> createState() => _ListDetailState();
}

class _ListDetailState extends State<ListDetailPage> {
  Completer<GoogleMapController> _controller = Completer();
  List<Marker> _markers = <Marker>[];

  late final LatLng _position =
      LatLng(double.parse(widget.model.lat), double.parse(widget.model.lng));

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initMarkers();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        title: Text(
          widget.model.sna,
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildMap(),
            _buildCard(),
          ],
        ),
      ),
    );
  }

  Widget _buildMap() {
    return Container(
      height: 300,
      child: GoogleMap(
        mapType: MapType.normal,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        initialCameraPosition: CameraPosition(zoom: 16, target: _position),
        onMapCreated: (controller) {
          _controller.complete(controller);
        },
        markers: Set<Marker>.of(_markers),
      ),
    );
  }

  void _initMarkers() {
    _markers.add(Marker(
        markerId: MarkerId('SomeId'),
        position: _position,
        infoWindow: InfoWindow(title: widget.model.sna)));
  }

  Widget _buildCard() {
    return Card(
      elevation: 5,
      margin: EdgeInsets.all(10),
      color: Colors.orangeAccent,
      child: Container(
        margin: EdgeInsets.all(10),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.model.sna),
            Text('地點：${widget.model.ar}'),
            Text('可借： ${widget.model.sbi} 台'),
            Text('可停： ${widget.model.bemp} 台')
          ],
        ),
      ),
    );
  }
}
