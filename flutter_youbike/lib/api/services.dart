
import 'package:flutter_youbike/models/youbike_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

abstract class YoubikesRepo {
  Future<YoubikeModel> getYoubikeList();
}

class YoubikeServices implements YoubikesRepo {
  static const _baseUrl = 'tcgbusfs.blob.core.windows.net';
  static const String _GET_YOUBIKE = '/blobyoubike/YouBikeTP.json';

  @override
  Future<YoubikeModel> getYoubikeList() async {
    // TODO: implement getYoubikeList
    Uri uri = Uri.https(_baseUrl, _GET_YOUBIKE);
    Response response = await http.get(uri);
    print(response.body);
    YoubikeModel youbikes = youbikeModelFromJson(response.body);
    return youbikes;
  }
}