import 'package:flutter/material.dart';
import 'package:flutter_youbike/list_detail/list_detail_page.dart';
import 'package:flutter_youbike/models/youbike_model.dart';

class ListRow extends StatelessWidget {
  final RetVal youbike;

  ListRow({required this.youbike});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.orangeAccent, borderRadius: BorderRadius.circular(5)),
        padding: EdgeInsets.all(20),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ListDetailPage(
                          model: youbike,
                        )));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(youbike.sna),
              Text('可借：${youbike.sbi} 台'),
              Align(
                alignment: Alignment.bottomRight,
                child: Text('更新時間：${youbike.mday}'),
              )
            ],
          ),
        ));
  }
}
