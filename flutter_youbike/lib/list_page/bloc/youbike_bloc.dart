import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_youbike/api/exceptions.dart';
import 'package:flutter_youbike/api/services.dart';
import 'package:flutter_youbike/models/youbike_model.dart';

part 'youbike_event.dart';

part 'youbike_state.dart';

class YoubikeBloc extends Bloc<YoubikeEvent, YoubikeState> {
  final YoubikesRepo youbikesRepo;
  YoubikeModel? youbikes;

  YoubikeBloc({required this.youbikesRepo}) : super(YoubikeInitial());

  @override
  Stream<YoubikeState> mapEventToState(YoubikeEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchYoubike) {
      yield YoubikeLoading();
      try {
        youbikes = await youbikesRepo.getYoubikeList();
        yield YoubikeLoaded(youbikes: youbikes!);
      } on SocketException {
        yield YoubikeListError(error: NoInternetException('No Intetnet'));
      } on HttpException {
        yield YoubikeListError(
            error: NoServiceFoundException('No Service Found'));
      } on FormatException {
        yield YoubikeListError(
            error: InvalidFormatException('Invalid Response format'));
      } catch (error) {
        yield YoubikeListError(error: UnknownException('Unknown error'));
      }
    }

    if (event is QueryYoubike) {
      var value = await _getSearchResults(event.model, event.searchText);
      yield QueryYoubikeState(youbikes: value);
    }
  }
  Future<Iterable<RetVal>> _getSearchResults(Iterable<RetVal> model, String query) async {
    var value = model.where((e) => e.sna.contains(query)).toList();
    return value;
  }
}
