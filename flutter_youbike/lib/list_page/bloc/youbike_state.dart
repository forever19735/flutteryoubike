part of 'youbike_bloc.dart';

abstract class YoubikeState extends Equatable {
  @override
  List<Object> get props => [];
}

class YoubikeInitial extends YoubikeState {}

class YoubikeLoading extends YoubikeState {}

class YoubikeLoaded extends YoubikeState {
  final YoubikeModel youbikes;
  YoubikeLoaded({required this.youbikes});
}

class YoubikeListError extends YoubikeState {
  final error;
  YoubikeListError({this.error});
}

class QueryYoubikeState extends YoubikeState {
  final Iterable<RetVal> youbikes;
  QueryYoubikeState({required this.youbikes});

  @override
  // TODO: implement props
  List<Object> get props => [youbikes];
}