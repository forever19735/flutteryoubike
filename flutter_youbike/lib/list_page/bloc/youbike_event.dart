part of 'youbike_bloc.dart';

abstract class YoubikeEvent extends Equatable {
  const YoubikeEvent();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class FetchYoubike extends YoubikeEvent {}

class QueryYoubike extends YoubikeEvent {
  String searchText = '';
  Iterable<RetVal> model;

  QueryYoubike(this.searchText, this.model);
}
