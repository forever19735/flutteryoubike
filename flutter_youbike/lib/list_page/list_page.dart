import 'package:flutter/material.dart';
import 'package:flutter_youbike/api/services.dart';
import 'package:flutter_youbike/list_page/list_row.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_youbike/models/youbike_model.dart';
import 'package:flutter_youbike/widget/loading.dart';
import 'package:flutter_youbike/list_page/bloc/youbike_bloc.dart';
import 'package:flutter_youbike/api/exceptions.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class YoubikeListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _YoubikeListState();
}

class _YoubikeListState extends State<YoubikeListPage> {
  late final _bloc;
  late Iterable<RetVal> model;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = BlocProvider.of<YoubikeBloc>(context);

    _loadYoubike();
  }

  Future<void> _loadYoubike() async {
    _bloc.add(FetchYoubike());
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BlocListener<YoubikeBloc, YoubikeState>(
      listener: (context, state) {},
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.black),
            backgroundColor: Colors.white,
            actions: [
              TextButton(
                  onPressed: () {
                    _loadYoubike();
                  },
                  style: ElevatedButton.styleFrom(primary: Colors.white),
                  child: Text(
                    '刷新',
                    style: TextStyle(color: Colors.black),
                  )),
            ],
          ),
          body: FloatingSearchAppBar(
            color: Colors.white,
            colorOnScroll: Colors.white,
            automaticallyImplyBackButton: false,
            transitionDuration: const Duration(milliseconds: 800),
            onQueryChanged: (value) {
              context.read<YoubikeBloc>().add(QueryYoubike(value, model));
            },
            body: SafeArea(child: _body()),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.vertical_align_top,
              color: Colors.black,
            ),
            backgroundColor: Colors.white,
            onPressed: () {
              _scrollController.animateTo(0.0,
                  duration: Duration(milliseconds: 300), curve: Curves.easeOut);
            },
          ),
        ),
      ),
    );
  }

  _body() {
    return Column(
      children: [
        BlocBuilder<YoubikeBloc, YoubikeState>(
            builder: (context, YoubikeState state) {
          print('更新啦');
          if (state is YoubikeListError) {
            final error = state.error;
            // String message = '${error.message}\nTap to Retry.';
            print(error);
          }

          if (state is YoubikeLoaded) {
            YoubikeModel youbikes = state.youbikes;
            model = youbikes.retVal.values.toList();
            return _list(youbikes.retVal.values.toList());
          }

          if (state is QueryYoubikeState) {
            List<RetVal> value = state.youbikes.toList();
            return _list(value);
          }

          return Loading();
        })
      ],
    );
  }

  Widget _list(List<RetVal> youbikes) {
    return Expanded(
        child: RefreshIndicator(
      onRefresh: _loadYoubike,
      child: ListView.builder(
          controller: _scrollController,
          itemCount: youbikes.length,
          itemBuilder: (_, index) {
            RetVal youbike = youbikes[index];
            return ListRow(youbike: youbike);
          }),
    ));
  }
}
