import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_youbike/api/services.dart';
import 'package:flutter_youbike/list_page/bloc/youbike_bloc.dart';
import 'package:flutter_youbike/list_page/list_page.dart';
import 'package:flutter_youbike/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: routes,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => YoubikeBloc(youbikesRepo: YoubikeServices()),
        child: YoubikeListPage(),
      ),
    );
  }
}
