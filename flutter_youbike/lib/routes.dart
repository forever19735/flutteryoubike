import 'package:flutter/widgets.dart';
import 'package:flutter_youbike/list_page/list_page.dart';

final Map<String, WidgetBuilder> routes = {
  (YoubikeListPage).toString(): (context) => YoubikeListPage(),
};