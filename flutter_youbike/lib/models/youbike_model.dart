// To parse this JSON data, do
//
//     final youbikeModel = youbikeModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

YoubikeModel youbikeModelFromJson(String str) => YoubikeModel.fromJson(json.decode(utf8.decode(str.runes.toList())));

String youbikeModelToJson(YoubikeModel data) => json.encode(data.toJson());

class YoubikeModel {
  YoubikeModel({
    required this.retCode,
    required this.retVal,
  });

  int retCode;
  Map<String, RetVal> retVal;

  factory YoubikeModel.fromJson(Map<String, dynamic> json) => YoubikeModel(
    retCode: json["retCode"],
    retVal: Map.from(json["retVal"]).map((k, v) => MapEntry<String, RetVal>(k, RetVal.fromJson(v))),
  );

  Map<String, dynamic> toJson() => {
    "retCode": retCode,
    "retVal": Map.from(retVal).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
  };
}

class RetVal {
  RetVal({
    required this.sno,
    required this.sna,
    required this.tot,
    required this.sbi,
    required this.mday,
    required this.lat,
    required this.lng,
    required this.ar,
    required this.snaen,
    required this.aren,
    required this.bemp,
    required this.act,
  });

  String sno;
  String sna;
  String tot;
  String sbi;
  String mday;
  String lat;
  String lng;
  String ar;
  String snaen;
  String aren;
  String bemp;
  String act;

  factory RetVal.fromJson(Map<String, dynamic> json) => RetVal(
    sno: json["sno"],
    sna: json["sna"],
    tot: json["tot"],
    sbi: json["sbi"],
    mday: json["mday"],
    lat: json["lat"],
    lng: json["lng"],
    ar: json["ar"],
    snaen: json["snaen"],
    aren: json["aren"],
    bemp: json["bemp"],
    act: json["act"],
  );

  Map<String, dynamic> toJson() => {
    "sno": sno,
    "sna": sna,
    "tot": tot,
    "sbi": sbi,
    "mday": mday,
    "lat": lat,
    "lng": lng,
    "ar": ar,
    "snaen": snaen,
    "aren": aren,
    "bemp": bemp,
    "act": act,
  };
}
